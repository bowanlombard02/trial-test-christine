/**
 *	Evaluates if a number is even or not
 *
 *	@function	isEven
 *	@param		number num
 *	@return		boolean
 */
function isEven(num) {
  return num % 2 === 0;
}