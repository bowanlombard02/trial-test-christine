/**
 *	Evaluates if a number is prime or not
 *
 *	@function	isPrime
 *	@param		number num
 *	@return		boolean
 */
function isPrime(num) {
  if (num < 2) {
    return false;
  }

  if (num === 2) {
    return true;
  } else if (num % 2 === 0) {
    return false;
  }
 
  for (var i = 3; i*i <= num; i += 2) {
    if (num % i === 0) {
      return false;
    }
  }
  return true;
}